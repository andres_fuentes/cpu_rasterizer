#ifndef _MEMORIA_
    #define _MEMORIA_
    #include <stdarg.h>
    #include <stdlib.h>
    #include <stdio.h>

    struct Node{
        void* val;
        size_t bytes;
        struct Node* next;
    };

    struct Slice{
        void** buffer;
        int size;
        int start;
        int end;
    };

    typedef struct Slice Context;

    void* newObject(size_t size);
    void* newArrayObject(size_t size, size_t type);
    Context* createContext();
    void setContext(Context* object);
    Context* getContext();
    void collectGarbage(Context* context);
    Context* deleteContext(Context* context);
#endif