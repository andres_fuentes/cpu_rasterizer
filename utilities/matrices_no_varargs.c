/**
 * Andres Fuentes Hernandez
 * Codigo para manejar memoria y matrices
 **/
#include "matrices_no_varargs.h"

Matrix2D* zeros(unsigned rows, unsigned  columns){
    Matrix2D* matriz = newObject(sizeof(Matrix2D));
    double* values = newArrayObject(rows*columns, sizeof(double));
    matriz->values = values;
    matriz->rows = rows;
    matriz->columns = columns;
    matriz->isTransposed = 0;
    return matriz;
}

Matrix2D* newMatrix(unsigned rows, unsigned  columns, Real a,Real b,Real c,Real d,Real e,Real f,Real g,Real h,Real i,Real j,Real k, Real l, Real m, Real n, Real o, Real p){
    Matrix2D* matriz = zeros( 4, 4);

    matriz->values[0] = a;
    matriz->values[1] = b;
    matriz->values[2] = c;
    matriz->values[3] = d;

    matriz->values[4] = e;
    matriz->values[5] = f;
    matriz->values[6] = g;
    matriz->values[7] = h;

    matriz->values[8] = i;
    matriz->values[9] = j;
    matriz->values[10] = k;
    matriz->values[11] = l;

    matriz->values[12] = m;
    matriz->values[13] = n;
    matriz->values[14] = o;
    matriz->values[15] = p;

    return matriz;
}

Matrix2D* newPoint(unsigned rows, unsigned  columns, Real x,Real y,Real z,Real q){
    Matrix2D* matriz = zeros( rows, columns);

    matriz->values[0] = x;
    matriz->values[1] = y;
    matriz->values[2] = z;
    matriz->values[3] = q;

    return matriz;
}

void printMatrix(Matrix2D* matriz){
    printf("\n<MATRIZ rows: %d, columns: %d, isTransposed: %d >\n", matriz->rows, matriz->columns, matriz->isTransposed);
    for(int i=0; i < matriz->rows; i++){
        for(int j=0; j < matriz->columns; j++){
        unsigned index = getIndex(matriz, i, j);
        printf("%.8f", matriz->values[index]);
        j == matriz->columns - 1 ? printf(";\n") : printf(",\t");
        }
    }
    printf("</MATRIZ>\n");
}

/*unsigned getIndex(Matrix2D** matriz, unsigned i, unsigned j){
    unsigned index;
    if(matriz->isTransposed == 0){
        index = j + i*matriz->columns;
    } else {
        index = i + j*matriz->rows;
    }
    return index;
}*/

Matrix2D* transpose(Matrix2D * matriz){
    int temp = matriz->columns;
    matriz->columns = matriz->rows;
    matriz->rows = temp;
    matriz->isTransposed = (1 - matriz->isTransposed);
    return matriz;
}


Matrix2D* add(Matrix2D *restrict matriz, double real){
    Matrix2D* resultado = zeros(matriz->rows, matriz->columns);
    resultado->isTransposed = matriz->isTransposed;
    unsigned int limite = matriz->rows*matriz->columns;
    for(int index=0;index<limite;index++){
        resultado->values[index] =  matriz->values[index] +  real;
    }

    return resultado;
}

Matrix2D* multiply(Matrix2D *restrict matriz, double real){
    Matrix2D* resultado = zeros( matriz->rows, matriz->columns);
    resultado->isTransposed = matriz->isTransposed;
    unsigned int limite = matriz->rows*matriz->columns;
    for(int index=0;index<limite;index++){
        resultado->values[index] = matriz->values[index] * real;
    }
    return resultado;
}
  

Matrix2D* divide(Matrix2D *restrict matriz, double real){
    Matrix2D* resultado = zeros(matriz->rows, matriz->columns);
    resultado->isTransposed = matriz->isTransposed;
    unsigned int limite = matriz->rows*matriz->columns;

    for(int index=0;index<limite;index++){
        resultado->values[index] = matriz->values[index] / real;    
    }
 

    return resultado;
}

Matrix2D* copy(Matrix2D *restrict matriz){
    Matrix2D* copy = zeros(matriz->rows, matriz->columns);
    unsigned int limite = matriz->rows*matriz->columns;
    for(int index=0;index<limite;index++){
        copy->values[index] = matriz->values[index];
    }
    copy->isTransposed = matriz->isTransposed;
    return copy;
}


Matrix2D* addMatrix(Matrix2D *restrict a, Matrix2D *restrict b){
    if(a->rows != b->rows || a->columns != b->columns){
        fprintf(stderr, "No es posible sumar las dimensiones no coinciden\n");
        exit(EXIT_FAILURE);
    }
    Matrix2D* result;
    result = zeros(a->rows, b->columns);
    for(int i=0; i < a->rows; i++){
        for(int j=0; j < a->columns; j++){
            unsigned indexA = getIndex(a, i, j);
            unsigned indexB = getIndex(b, i, j);
            unsigned indexR = getIndex(result, i, j);
            result->values[indexR] = a->values[indexA] + b->values[indexB];
        }
    }
    return result;
}

Matrix2D* subMatrix(Matrix2D *restrict a, Matrix2D *restrict b){
    if(a->rows != b->rows || a->columns != b->columns){
        fprintf(stderr, "No es posible sumar las dimensiones no coinciden\n");
        exit(EXIT_FAILURE);
    }
    Matrix2D* result;
    result = zeros(a->rows, b->columns);
    for(int i=0; i < a->rows; i++){
        for(int j=0; j < a->columns; j++){
            unsigned indexA = getIndex(a, i, j);
            unsigned indexB = getIndex(b, i, j);
            unsigned indexR = getIndex(result, i, j);
            result->values[indexR] = a->values[indexA] - b->values[indexB];
        }
    }
    return result;
}


Matrix2D* multiplyMatrix(Matrix2D *restrict a, Matrix2D *restrict b){
    if(a->columns != b->rows){
        fprintf(stderr, "No es posible multiplicar las dimensiones no coinciden\n");
        exit(EXIT_FAILURE);
    }
    Matrix2D* result;
    result = zeros(a->rows, b->columns);
    for(int i=0; i < a->rows; i++){
        for(int k=0; k < b->columns; k++){
            unsigned indexR = getIndex(result, i, k);
            for(int j=0; j < a->columns; j++){
                unsigned indexA = getIndex(a, i, j);
                unsigned indexB = getIndex(b, j, k);
                result->values[indexR] += a->values[indexA] * b->values[indexB];
            }
        }
    }
    return result;
}

Point* cross(Point *restrict a, Point *restrict b){
    if((a->rows != 4 && a->columns != 1)){
        fprintf(stderr, "El punto a no es valido\n");
        exit(EXIT_FAILURE);
    }
    if((b->rows != 4 && b->columns != 1)){
        fprintf(stderr, "El punto b no es valido\n");
        exit(EXIT_FAILURE);
    }
    //– (xx´ + yy´ + zz´) + (yz´ – zy´)i +(zx´ – xz´)j + (xy´ – yx´)k
    double x = getY(a)*getZ(b) - getZ(a)*getY(b);
    double y = getZ(a)*getX(b) - getX(a)*getZ(b);
    double z = getX(a)*getY(b) - getY(a)*getX(b);
    double w = dot(a,b);
    return getPoint(x,y,z,w);
}

double dot(Point *restrict a, Point *restrict b){
    if((a->rows != 4 && a->columns != 1)){
        fprintf(stderr, "El punto a no es valido o esta transpuesto\n");
        exit(EXIT_FAILURE);
    }
    if((b->rows != 4 && b->columns != 1)){
        fprintf(stderr, "El punto b no es valido o esta transpuesto\n");
        exit(EXIT_FAILURE);
    }
    double result = (getX(a)*getX(b)) + (getY(a)*getY(b)) + (getZ(a)*getZ(b)); 
    return result;
}


Point* normalize(Point *restrict a){
    Point* b = copy(a);
    double abs = sqrt((getX(b)*getX(b)) + (getY(b)*getY(b)) + (getZ(b)*getZ(b)));
    setX(b, getX(b)/abs);
    setY(b, getY(b)/abs);
    setZ(b, getZ(b)/abs);
    return b;
}