#include "objects3D.h"
static char isInsdePolygon(double x, double y, Face* face, Object3D* object);
static double triangleArea(Pixel a, Pixel b, Pixel c);

Transform* getLookAt(Camara* camara, Point* target){

    Transform* rotacionZ = getTransform(
        cos(camara->rotZ), -sin(camara->rotZ),  0.0, 0.0,
        sin(camara->rotZ), cos(camara->rotZ),  0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0
    );

    Transform* rotacionY = getTransform(
        cos(camara->rotY), 0.0, sin(camara->rotY), 0.0,
        0.0, 1.0, 0.0, 0.0,
        -sin(camara->rotY), 0.0, cos(camara->rotY), 0.0,
        0.0, 0.0, 0.0, 1.0     
    );

    Transform* rotacionX = getTransform(
        1.0, 0.0, 0.0, 0.0,
        0.0, cos(camara->rotX), sin(camara->rotX), 0.0,
        0.0, -sin(camara->rotX), cos(camara->rotX), 0.0,
        0.0, 0.0, 0.0, 1.0
    );

    Transform* rotacion = multiplyMatrix(rotacionZ, multiplyMatrix(rotacionY, rotacionX));
    Point* camaraLocal = multiplyMatrix(rotacion, camara->posicion);
    double x = getX(camaraLocal)-getX(target);
    double y = getY(camaraLocal)-getY(target);
    double z = getZ(camaraLocal)-getZ(target);
    Point* forward = normalize(getPoint(x, y, z, 1.0));
    Point* up = normalize(multiplyMatrix(rotacion, camara->up));
    Point* side = cross(up, forward);
    
    Transform* lookAt = multiplyMatrix(
    getTransform(
        getX(side), getY(side), getZ(side), 0.0,
        getX(up), getY(up), getZ(up), 0.0,
        getX(forward), getY(forward), getZ(forward), 0.0,
        0.0, 0.0, 0.0, 1.0
    ),
    getTransform(
        1.0, 0.0, 0.0, -1*getX(camaraLocal),
        0.0, 1.0, 0.0, -1*getY(camaraLocal),
        0.0, 0.0, 1.0, -1*getZ(camaraLocal),
        0.0, 0.0, 0.0, 1.0
    ));

    return lookAt;
}

Transform* getObjectTransformation(Object3D* objecto){
        Transform* rotacionZ = getTransform(
        cos(objecto->rotZ), -1*sin(objecto->rotZ),  0.0, 0.0,
        sin(objecto->rotZ), cos(objecto->rotZ),  0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0
    );

    Transform* rotacionY = getTransform(
        cos(objecto->rotY), 0.0, 1*sin(objecto->rotY), 0.0,
        0.0, 1.0, 0.0, 0.0,
        -sin(objecto->rotY), 0.0, cos(objecto->rotY), 0.0,
        0.0, 0.0, 0.0, 1.0     
    );

    Transform* rotacionX = getTransform(
        1.0, 0.0, 0.0, 0.0,
        0.0, cos(objecto->rotX), -1*sin(objecto->rotX), 0.0,
        0.0, sin(objecto->rotX), cos(objecto->rotX), 0.0,
        0.0, 0.0, 0.0, 1.0
    );
    Transform* translacion = getTransform(
        1.0, 0.0, 0.0, getX(objecto->origen),
        0.0, 1.0, 0.0, getY(objecto->origen),
        0.0, 0.0, 1.0, getZ(objecto->origen),
        0.0, 0.0, 0.0, 1.0
    );
    Transform* rotacion = multiplyMatrix(rotacionX, multiplyMatrix(rotacionY, rotacionZ));
    Transform* final = multiplyMatrix(rotacion,translacion);
    return final;
}

Transform* getPerspectiveProeyctionMatrix(Camara* camara, double aspect){
    double f = 1/tan(camara->fov/2);
    double a = camara->farPlane /(camara->nearPlane - camara->farPlane);
    double b = camara->nearPlane /(camara->nearPlane - camara->farPlane);

    return getTransform(
        f*(1/aspect), 0.0, 0.0, 0.0,
        0.0, f, 0.0, 0.0,
        0.0, 0.0, a+b, 2*camara->nearPlane*a,
        0.0, 0.0, -1.0, 0.0
    );

}

Transform* getOrtogonalProeyctionMatrix(Camara* camara, double aspect){
    double a = 1.0/(camara->nearPlane - camara->farPlane);
    double b = 1.0/(camara->rightPlane - camara->leftPlane);
    double c = 1.0/(camara->topPlane - camara->bottomPlane);

    return getTransform(
        2.0*b/aspect, 0.0, 0.0, 0.0,//-b*(camara->rightPlane + camara->leftPlane),
        0.0, 2.0*c, 0.0, 0.0,//-c*(camara->topPlane + camara->bottomPlane),
        0.0, 0.0, 2.*a, -a*(camara->nearPlane + camara->farPlane),
        0.0, 0.0, 0.0, 1.0
    );

}

Transform* getDeviceMatrix(double aspect, int width, int height){
    double scale =height/2.0;
    Transform* pantalla = getTransform( 
        scale*aspect, 0.0, 0.0, scale*aspect,
        0.0, -1*scale, 0.0, 1*scale,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0
    );
    return pantalla;
}

Object3D* newEmptyObject3D(int numVertex, int numFaces){
    Object3D* object = newObject(sizeof(Object3D));
    object->vertex = newArrayObject(numVertex, sizeof(Point*));
    object->faces = newArrayObject(numFaces, sizeof(Face*));
    object->colors = newArrayObject(numVertex, sizeof(Color));
    object->origen = getPoint(0.0, 0.0, 0.0, 1.0);
    object->numVertex = numVertex;
    object->numFaces = numFaces;
    object->rotX = 0.0;
    object->rotY = 0.0;
    object->rotZ = 0.0;
    object->ka = 0.5;
    object->kd = 1.0;
    object->ks = 2.0;
    object->ms = 6.0;
    return object;
}

Object3D* copyObject3D(Object3D* from){
    Object3D* object = newEmptyObject3D(from->numVertex, from->numFaces);
    object->vertex = from->vertex;
    object->faces = from->faces;
    object->normals = from->normals;
    object->colors = from->colors;
    object->origen = from->origen;
    object->numVertex = from->numVertex;
    object->numFaces = from->numFaces;
    object->rotX = from->rotX;
    object->rotY = from->rotY;
    object->rotZ = from->rotZ;
    object->textels = from->textels;
    return object;
}

Face* newFace(int size, ...){
    Face* face = newObject(sizeof(Face));
    face->vertexIndex = newArrayObject(size, sizeof(int));
    face->size = size;
    va_list list;
    va_start(list, size);
    for(int index = 0; index < size ;index++){
        face->vertexIndex[index] = va_arg(list, int);
    }
    va_end(list);
    return face;
}

/*
    N = {0,0,0};
    for (each vertex in the polygon Vn)
    {
    Nx += (Vny - V(n+1)y) * (Vnz + V(n+1)z);
    Ny += (Vnz - V(n+1)z) * (Vnx + V(n+1)x);
    Nz += (Vnx - V(n+1)x) * (Vny + V(n+1)y);
    }
    */
void createVertexNormals(Object3D* object){

    object->normals = newArrayObject(object->numVertex, sizeof(Point*));

    Context* externalContext = getContext();
    Context* functioncontext = createContext();
    setContext(functioncontext);
    Point** normals = newArrayObject(object->numVertex, sizeof(Point*));
    Point** faceNormals =   newArrayObject(object->numFaces, sizeof(Point*));
    for(int i=0; i < object->numVertex; i++){
        normals[i] = getPoint(0.0, 0.0, 0.0, 0.0);
    }

    for(int i=0; i < object->numFaces; i++){
        Face* face = object->faces[i];
        Point* vertex01 = object->vertex[face->vertexIndex[0]];
        Point* vertex02 = object->vertex[face->vertexIndex[1]];
        Point* vertex03 = object->vertex[face->vertexIndex[2]];
        faceNormals[i] = cross( normalize(subMatrix(vertex02, vertex01)),  normalize(subMatrix(vertex03, vertex01)));
        double angulos[3];
        angulos[0] = acos(dot(normalize(subMatrix(vertex02, vertex01)),normalize(subMatrix(vertex03, vertex01))));
        angulos[1] = acos(dot(normalize(subMatrix(vertex03, vertex02)),normalize(subMatrix(vertex01, vertex02))));
        angulos[2] = acos(dot(normalize(subMatrix(vertex01, vertex03)),normalize(subMatrix(vertex02, vertex03))));
        for(int j=0; j < (face->size); j++){
            normals[face->vertexIndex[j]] = addMatrix( multiply(faceNormals[i],angulos[j]), normals[face->vertexIndex[j]]);
        }
    }
    setContext(externalContext);

    for(int i=0; i < object->numVertex; i++){
        object->normals[i] = normalize(normals[i]);
        if(isnan(getX(object->normals[i]))){
            object->normals[i] = getPoint(0.0,0.0,0.0,0.0); //detectando nan si el compilador es ieee
        }
        setW(object->normals[i], 0.0);
    }
    
    setContext(functioncontext);
    deleteContext(functioncontext);
    setContext(externalContext);
}

double* createZbuffer(int width, int height){
    double* zbuffer = newArrayObject(width*height, sizeof(double));
    int maximo = width*height;
    for(int i=0; i < maximo; i++){
        zbuffer[i] = INT_MAX;
    }
    return zbuffer;
}

char* createScreenBuffer(int width, int height){
    char* screen = newArrayObject(width*height*4, sizeof(char));
    return screen;
}


void vertexShading(ShaderContext* context){
    Object3D* baseObject = context->object;
    Transform*  lookAt = context->lookAt;
    Transform*  perspective = context->perspective;
    Transform*  deviceMatrix = context->deviceMatrix;
    Object3D* object = copyObject3D(baseObject);
    Point** puntosProyectados = newArrayObject(baseObject->numVertex, sizeof(Point*));
    Point** normalesTransformadas = newArrayObject(baseObject->numVertex, sizeof(Point*));
    Light** diffuseLightsTransformadas = newArrayObject(context->numDiffuseLights, sizeof(Light*));
    Transform* localTransform = getObjectTransformation(baseObject);
    Transform* viewMatrix = multiplyMatrix(lookAt,localTransform);
    Transform* projectionMatrix = multiplyMatrix(multiplyMatrix(deviceMatrix,perspective),viewMatrix);

    for(int i = 0; i < baseObject->numVertex;i++){
        puntosProyectados[i] = multiplyMatrix(projectionMatrix,baseObject->vertex[i] );
        double savedW = getW(puntosProyectados[i]);
        puntosProyectados[i] = divide(puntosProyectados[i],savedW);
        setW(puntosProyectados[i], savedW);
        normalesTransformadas[i] = multiplyMatrix(viewMatrix, baseObject->normals[i]);
    }


    for(int i = 0; i < context->numDiffuseLights ;i++){
        Light* light = newObject(sizeof(Light));
        light->color = context->diffuseLights[i]->color;
        light->origen = multiplyMatrix(lookAt, context->diffuseLights[i]->origen);
        light->normal = multiplyMatrix(lookAt, context->diffuseLights[i]->normal);
        diffuseLightsTransformadas[i] = light;
    }

    object->normals = normalesTransformadas;
    object->vertex = puntosProyectados;
    context->diffuseLights = diffuseLightsTransformadas;
    context->object = object;
    if(context->shading == GOURAUD){
        Color* colores = newArrayObject(object->numVertex, sizeof(Color));
        for(int i=0; i < object->numVertex; i++){
            Point* normalP = object->normals[i];
            colores[i] = renderColor(object->colors[i],normalP, object->vertex[i],context);
        }
        object->colors = colores;
    }
}

void fragmentShading(ShaderContext* context){
    Object3D* transformedObject = context->object;
    int width = context->width;
    int height = context->height;
    char* screenBuffer = context->screenbuffer;
    double* zbuffer = context->zbuffer;
    //Find bound of face
    for(int i=0; i < transformedObject->numFaces ; i++){
        Face* face = transformedObject->faces[i];
        Box2D box = {DBL_MIN, DBL_MAX, DBL_MIN, DBL_MAX};
        for(int j=0;j < face->size; j++){
            box.maxX =  max(box.maxX, getX(transformedObject->vertex[face->vertexIndex[j]]));
            box.maxY = max(box.maxY, getY(transformedObject->vertex[face->vertexIndex[j]]));
            box.minX =  min(box.minX, getX(transformedObject->vertex[face->vertexIndex[j]]));
            box.minY =  min(box.minY, getY(transformedObject->vertex[face->vertexIndex[j]]));
        }
        int endX = box.maxX < width ? (int)box.maxX + 1 : width-1;
        int endY  = box.maxY < height ? (int)box.maxY + 1 : height-1;
        int startX = box.minX > 0 ? (int)box.minX - 1 : 0;
        int startY = box.minY > 0 ? (int)box.minY - 1 : 0;
        Pixel a = { getX(transformedObject->vertex[face->vertexIndex[0]]), getY(transformedObject->vertex[face->vertexIndex[0]])};
        Pixel b = { getX(transformedObject->vertex[face->vertexIndex[1]]), getY(transformedObject->vertex[face->vertexIndex[1]])};
        Pixel c = { getX(transformedObject->vertex[face->vertexIndex[2]]), getY(transformedObject->vertex[face->vertexIndex[2]])};
        double totalArea = triangleArea(a,b,c);
        Point* normalA = transformedObject->normals[face->vertexIndex[0]];
        Point* normalB = transformedObject->normals[face->vertexIndex[1]];
        Point* normalC = transformedObject->normals[face->vertexIndex[2]];
                    
        for(int x = startX; x <= endX; x++){
            for(int y = startY; y <= endY; y++){
                char isInside = isInsdePolygon(x + 0.5, y + 0.5, face, transformedObject);
                if(isInside){
                    Point* va = transformedObject->vertex[face->vertexIndex[0]];
                    Point* vb = transformedObject->vertex[face->vertexIndex[1]];
                    Point* vc = transformedObject->vertex[face->vertexIndex[2]];
                    Pixel p = {x, y};
                    double u = (triangleArea(b,c,p)/totalArea);
                    double v = (triangleArea(a,c,p)/totalArea);
                    double w = (triangleArea(a,b,p)/totalArea);
                    double total = u + v + w;
                    if(total > 1.0){
                        u = u/total;
                        v = v/total;
                        w = w/total;
                    }
                    double up = (u/getW(va))/((u/getW(va)) + (v/getW(vb)) + (w/getW(vc)));
                    double vp = (v/getW(vb))/((u/getW(va)) + (v/getW(vb)) + (w/getW(vc)));
                    double wp = (w/getW(vc))/((u/getW(va)) + (v/getW(vb)) + (w/getW(vc)));
                    double Zp = up*getZ(va) + vp*getZ(vb) + wp*getZ(vc);
                    if( (zbuffer[getBufferIndex(x, y, height)] -Zp) > 0.0){
                        double Xp = up*getX(va) + vp*getX(vb) + wp*getX(vc);
                        double Yp = up*getY(va) + vp*getY(vb) + wp*getY(vc);
                        Point*  punto = getPoint(Xp,Yp,Zp,1.0);
                        zbuffer[getBufferIndex(x, y, height)] = Zp;
                        Point* ta = transformedObject->textels[face->vertexIndex[0]];
                        Point* tb = transformedObject->textels[face->vertexIndex[1]];
                        Point* tc = transformedObject->textels[face->vertexIndex[2]];
                        double Xt = up*getX(ta) + vp*getX(tb) + wp*getX(tc);
                        double Yt = up*getY(ta) + vp*getY(tb) + wp*getY(tc);
                        double Zt = up*getZ(ta) + vp*getZ(tb) + wp*getZ(tc);
                        long Ca = transformedObject->colors[face->vertexIndex[0]];
                        long Cb = transformedObject->colors[face->vertexIndex[1]];
                        long Cc = transformedObject->colors[face->vertexIndex[2]];
                        double Rp = (up*Rn(Ca) + vp*Rn(Cb) + wp*Rn(Cc));
                        double Gp = (up*Gn(Ca) + vp*Gn(Cb) + wp*Gn(Cc));
                        double Bp = (up*Bn(Ca) + vp*Bn(Cb) + wp*Bn(Cc));
                        Color pixelColor = RGBn(Rp,Gp,Bp);
                        if(context->shading == PHONG){
                            Point* normalP =  normalize(addMatrix(addMatrix(multiply(normalA, up), multiply(normalB,vp)), multiply(normalC,wp)));
                            Color finalColor = context->textureOn? mixColors(getTexture(Xt,Yt,Zt),pixelColor):pixelColor;
                            Color  renderizedColor = renderColor(finalColor, normalP, punto, context);
                            screenBuffer[getImageIndex(x, y, 0,width, height)] = R(renderizedColor);
                            screenBuffer[getImageIndex(x, y, 1,width, height)] = G(renderizedColor);
                            screenBuffer[getImageIndex(x, y, 2,width, height)] = B(renderizedColor);
                        } else if (context->shading == GOURAUD){
                            Color finalColor = context->textureOn? mixColors(getTexture(Xt,Yt,Zt),pixelColor):pixelColor;
                            screenBuffer[getImageIndex(x, y, 0,width, height)] = R(finalColor);
                            screenBuffer[getImageIndex(x, y, 1,width, height)] = G(finalColor);
                            screenBuffer[getImageIndex(x, y, 2,width, height)] = B(finalColor);
                        } else if(context->shading == FLAT){            
                            Point* faceNormal = normalize(addMatrix(addMatrix(normalA, normalB),normalC));
                            long Ca = transformedObject->colors[face->vertexIndex[0]];
                            double xc = (getX(va) + getX(vb) + getX(vc) )/ 3.0;
                            double yc = (getY(va) + getY(vb) + getY(vc) )/ 3.0;
                            double zc = (getZ(va) + getZ(vb) + getZ(vc) )/ 3.0;
                            Point* punto = getPoint(xc, yc, zc, 1.0);
                            Color colorCara = renderColor(Ca,faceNormal, punto, context);
                            Color finalColor = context->textureOn? mixColors(getTexture(Xt,Yt,Zt),colorCara):colorCara;
                            screenBuffer[getImageIndex(x, y, 0,width, height)] = R(finalColor);
                            screenBuffer[getImageIndex(x, y, 1,width, height)] = G(finalColor);
                            screenBuffer[getImageIndex(x, y, 2,width, height)] = B(finalColor);
                        }
                    }
                }
            }
        }
    }
}

Color renderColor(Color color, Point *restrict normal, Point *restrict eye, ShaderContext* context){
    Light* ambientLight = context->ambientLight;
    Light** diffuseLights = context->diffuseLights;
    Object3D* object = context->object;
    double Rp = Rn(color);
    double Gp = Gn(color);
    double Bp = Bn(color);
    double Ir = Rp*object->ka*(Rn(ambientLight->color));
    double Ig = Gp*object->ka*(Gn(ambientLight->color));
    double Ib = Bp*object->ka*(Bn(ambientLight->color));
    for(int k=0; k < context->numDiffuseLights; k++){
        double lightIntesity = dot(diffuseLights[k]->normal, normal);
        lightIntesity = lightIntesity < 0.0? 0.0:lightIntesity;
        if(context->diffuseOn){
            Ir += Rp*object->kd*(Rn(diffuseLights[k]->color)) * lightIntesity;
            Ig += Gp*object->kd*(Gn(diffuseLights[k]->color)) * lightIntesity;
            Ib += Bp*object->kd*(Bn(diffuseLights[k]->color)) * lightIntesity;
        }

        if(context->specularOn){
            Point* toLight = diffuseLights[k]->normal;
            Point* v = normalize(eye);
            Point* r = normalize(subMatrix(multiply(normal, 2.0*dot(normal, toLight)),toLight));
            double reflection = dot(v, r);
            reflection = reflection < 0.0? 0.0: pow(reflection,object->ms);
            Ir += Rp*object->ks*(Rn(diffuseLights[k]->color)) * reflection * lightIntesity;
            Ig += Gp*object->ks*(Gn(diffuseLights[k]->color)) * reflection * lightIntesity;
            Ib += Bp*object->ks*(Bn(diffuseLights[k]->color)) * reflection * lightIntesity;
        }
        
    }

    Ir = Ir /2.0;
    Ig = Ig /2.0;
    Ib = Ib /2.0;
    Ir = Ir > 1.0? 1.0:Ir;
    Ig = Ig > 1.0? 1.0:Ig;
    Ib = Ib > 1.0? 1.0:Ib;
    return RGBn(Ir, Ig, Ib);
}

static char isInsdePolygon(double x, double y, Face* face, Object3D* object){
    char isInsideCW = 1;
    char isInsideCCW = 1;
    for(int j=0; j < face->size; j++){
        Point* vertex01 = object->vertex[face->vertexIndex[j]];
        Point* vertex02; 
        if( j == face->size - 1){
            vertex02 = object->vertex[face->vertexIndex[0]];
        } else {
            vertex02 = object->vertex[face->vertexIndex[j+1]];
        }

        double x0 = getX(vertex01);
        double y0 = getY(vertex01);

        double x1 = getX(vertex02);
        double y1 = getY(vertex02);

        double sum = (y - y0)*(x1 - x0) - (x - x0)*(y1 - y0);
        if( sum > error ){
            isInsideCW &= 1;
            isInsideCCW &= 0;
        } else {
            isInsideCW &= 0;
            isInsideCCW &= 1;
        }
    }
    return    isInsideCCW;
}

void triangulateConvexFaces(Object3D* object){
    int newSize = 0;
    for(int i=0; i < object->numFaces; i++){
        Face* face = object->faces[i];
        newSize += (face->size - 2); 
    }
    Face** triangulated = newArrayObject(newSize, sizeof(Face*));
    int triangleIndex = 0;
    for(int i=0; i < object->numFaces; i++){
        Face* face = object->faces[i];
        for(int j=1;j < face->size - 1; j++){
            Face* triangle = newFace(3, face->vertexIndex[0], face->vertexIndex[j], face->vertexIndex[j+1]);
            triangulated[triangleIndex] = triangle;
            triangleIndex++;
        }
    }
    object->numFaces = newSize;
    object->faces = triangulated;
}

static double triangleArea(Pixel a, Pixel b, Pixel c){
    Pixel ab = {a.x - b.x, a.y - b.y};
    Pixel cb = {c.x - b.x, c.y - b.y};
    double area = fabs(ab.x*cb.y - cb.x*ab.y);
    return area;
}

Object3D* readOBJFile(char* fileName){
    Context* externalContext = getContext();
    Context* functioncontext = createContext();
    setContext(functioncontext);
    FILE *file = fopen(fileName, "r");
    if(file == NULL){
    fprintf(stderr, "No pude leer el archivo\n");
        exit(EXIT_FAILURE); 
    }
    const int MAX_LINE = 1000;
    char* line = newArrayObject(MAX_LINE, sizeof(char));
    List* headVertex = newObject(sizeof(List));
    List* currentVertex = headVertex;
    List* headFace = newObject(sizeof(List));
    List* currentFace = headFace;
    int vertexCount = 0;
    int faceCount = 0;
    printf("Empezando a leer el archivo %s\n", fileName);
    while(fgets(line, MAX_LINE, file) != NULL){
        if(line[0] == '#'){
            continue;
        }
        if(line[0] == 'v'){
            if(line[1] != 't' && line[1] != 'n' ){
                double x;
                double y;
                double z;
                sscanf((line+1), "%lf %lf %lf", &x, &y, &z);
                Point* punto = getPoint(x, y, z, 1.0);
                
                vertexCount++;
                currentVertex->val = punto;
                currentVertex->next = newObject(sizeof(List));
                currentVertex = currentVertex->next;
                //printf("%s", line);
            }
        }
        if(line[0] == 'f'){
            //clean faces no leo ni normales, solo definicion de las caras
            int index = 1;
            char state = 'S';
            while(line[index] != '\0'){
                switch (state)
                {
                case 'S':
                    if(line[index] == '/'){
                        line[index] = ' ';
                        state ='T';
                    } 
                break;
                case 'T':
                    if(line[index] == '/'){
                        state ='N';
                    } else if(line[index] == ' '){
                        state ='S';
                    }
                    line[index] = ' ';
                break;
                case 'N':
                    if(line[index] == ' '){
                        state ='S';
                    } 
                    line[index] = line[index] == '\n'? line[index] : ' ';
                break;
                default:
                    break;
                }
                index++;
            }
            faceCount++;
            int vertex[4]; //solo puedo leer o cruadados o triangulos
            int succesfull = sscanf((line+1), "%d %d %d %d", &vertex[0], &vertex[1], &vertex[2], &vertex[3]);
            //printf(line);
            if(succesfull == 4){ //Si el ultimo valor no se modifico enotnces es un triangulo
                Face* face = newFace(4, 
                vertex[0] < 0? vertex[0]+vertexCount:(vertex[0]-1), 
                vertex[1] < 0? vertex[1]+vertexCount:(vertex[1]-1), 
                vertex[2] < 0? vertex[2]+vertexCount:(vertex[2]-1),
                vertex[3] < 0? vertex[3]+vertexCount:(vertex[3]-1));
                currentFace->val = face;
                currentFace->next = newObject(sizeof(List));
                currentFace = currentFace->next;
            } else {
                Face* face = newFace(3,
                vertex[0] < 0? vertex[0]+vertexCount:(vertex[0]-1), 
                vertex[1] < 0? vertex[1]+vertexCount:(vertex[1]-1), 
                vertex[2] < 0? vertex[2]+vertexCount:(vertex[2]-1));
                currentFace->val = face;
                currentFace->next = newObject(sizeof(List));
                currentFace = currentFace->next;
            }
            //printf("%s", line);
            
        }
    }
    setContext(externalContext);
    Object3D* object = newEmptyObject3D(vertexCount,faceCount);
    currentVertex = headVertex;
    for(int i=0;i<vertexCount; i++){
        object->vertex[i] = copy(currentVertex->val);
        object->colors[i] = RGB(255,255,255);
        currentVertex = currentVertex->next;

    }
    printf("Vertices agregados: %d\n", vertexCount);
    printf("Caras agregadas: %d\n", faceCount);
    currentFace = headFace;
    for(int i=0;i<faceCount; i++){
        object->faces[i] = copyFace(currentFace->val);
        currentFace = currentFace->next;
    }
    triangulateConvexFaces(object);
    printf("Caras trianguladas\n");
    createVertexNormals(object);
    createTextureCoordinates(object);
    setContext(functioncontext);
    deleteContext(functioncontext);
    setContext(externalContext);
    fclose(file);
    return object;
}

Face* copyFace(Face* face){
    Face* copia = newObject(sizeof(Face));
    copia->vertexIndex =face->vertexIndex;//newArrayObject(face->size, sizeof(int));
    copia->size = face->size;
    copia->vertexIndex =face->vertexIndex;
    return copia;
}

void repositionObjectCamara(Object3D* object, Camara* camara){
    double maxX = object->box->maxX ;
    double maxY = object->box->maxY ;
    double maxZ = object->box->maxZ ;

    double minX = object->box->minX ; 
    double minY = object->box->minY ; 
    double minZ = object->box->minZ ; 
    double oX = minX + (maxX - minX)/2.0;
    double oY = minY + (maxY - minY)/2.0;
    double oZ = minZ + (maxZ - minZ)/2.0;
    printf("%lf, %lf, %lf\n", oX, oY, oZ);
    object->origen = getPoint( -oX,  -oY, -oZ, 1.0);
    double distX = maxX - minX;
    double distY = maxY - minY;
    double distZ = maxZ - minZ;
    double biggest = max(max(distX,distY),distZ);
    camara->posicion = getPoint(0.0, 0.0, 1.5*biggest, 1.0);
    camara->nearPlane = biggest/1.5;

    double ancho = 1.0*biggest;
    camara->leftPlane = -ancho;
    camara->rightPlane = ancho;
    camara->bottomPlane = -ancho;
    camara->topPlane = ancho;    
}

void createTextureCoordinates(Object3D* object){
     double maxX = DBL_MIN ;
    double maxY = DBL_MIN;
    double maxZ = DBL_MIN;

    double minX = DBL_MAX ;
    double minY = DBL_MAX ;
    double minZ = DBL_MAX ;
    for(int j=0;j < object->numVertex; j++){
        maxX = max(maxX, getX(object->vertex[j]));
        maxY = max(maxY, getY(object->vertex[j]));
        maxZ = max(maxZ, getZ(object->vertex[j]));
        minX =  min(minX, getX(object->vertex[j]));
        minY = min(minY, getY(object->vertex[j]));
        minZ = min(minZ, getZ(object->vertex[j]));
    }
    Box3D* box = newObject(sizeof(Box3D));
    box->maxX = maxX;
    box->maxY = maxY;
    box->maxZ = maxZ;
    box->minX = minX;
    box->minY = minY;
    box->minZ = minZ;
    object->box = box;
    double distX = box->maxX - box->minX;
    double distY = box->maxY - box->minY;
    double distZ = box->maxZ - box->minZ;
    object->textels = newArrayObject(object->numVertex,sizeof(Point*));
    for(int j=0;j < object->numVertex; j++){
        Point* textel = getPoint((getX(object->vertex[j])-box->minX)/distX, (getY(object->vertex[j])-box->minY)/distY, (getZ(object->vertex[j])-box->minZ)/distZ, 0.0);
        object->textels[j] = textel;
    }
}

Color getTexture(double Xt, double Yt, double Zt){

        Color madera01 = RGB(174,143,96);
        Color madera02 = RGB(102,51,0);
        /*if(inside <  0.0){
            madera = RGB(102,51,0);
        }*/
        
        double T = 0.5*((Xt-0.5)*(Xt-0.5) + (Yt-0.5)*(Yt-0.5)) + 0.5*sin(6*M_PI*(Zt+0.2*Yt));
        
        double funcionMadera = fabs(0.5*cos(7*M_PI*(T+Zt))) + 0.5 ;
        double Ir = (Rn(madera01)*funcionMadera + Rn(madera02)*(1.0-funcionMadera));
        double Ig = (Gn(madera01)*funcionMadera + Gn(madera02)*(1.0-funcionMadera));
        double Ib = (Bn(madera01)*funcionMadera + Bn(madera02)*(1.0-funcionMadera));
        return RGBn(Ir,Ig,Ib);
        
}