#include "memoria.h"

/*
Recordar que al liberar objetos usando free, manualmente debo hacer el pointer apuntar a null
de igual manera malloc usa el objeto no el puntero al objeto.
Al crear definiciones de tipo, no enmascarar punteros porque peude derivar en problemas
 */

static Context* currentContext;
static size_t memory = 0;
static void pushObjectList(void* object, size_t size);

void* newObject(size_t size){
    void* object = malloc(size);
    if(object == NULL){
        fprintf(stderr, "Ya no hay memoria\n");
        exit(EXIT_FAILURE);
    }
    memory += size;
    pushObjectList(object, size);
    return object;
}

void* newArrayObject(size_t size, size_t type){
    void* object = calloc(size, type);
    if(object == NULL){
        fprintf(stderr, "Ya no hay memoria pal array :(\n");
        exit(EXIT_FAILURE);
    }
    memory += (size*type);
    pushObjectList(object, size*type);
    return object;
}

Context* createContext(){
    Context* stackContext = malloc(sizeof(Context)); 
    void** headObject = calloc(sizeof(char*), 10);
    if(headObject == NULL){
        fprintf(stderr, "No hay memoria para el puntero\n");
        exit(EXIT_FAILURE);
    }
    stackContext->start = 0;
    stackContext->end = 1;
    stackContext->size = 10;
    stackContext->buffer = headObject;
    memory += sizeof(Context) + sizeof(headObject);
    return stackContext;
}

void setContext(Context* context){
    currentContext = context;
}

Context* getContext(){
    return currentContext;
}

static void pushObjectList(void* object, size_t size){
    if((currentContext->size) == currentContext->end){
        void** newBuffer = realloc(currentContext->buffer, sizeof(char*)*currentContext->size * 2);
        if(newBuffer == NULL){
            fprintf(stderr, "No hay memoria para el puntero\n");
            exit(EXIT_FAILURE);
        }
        currentContext->buffer = newBuffer;
        currentContext->size = currentContext->size*2;
        memory +=  currentContext->size;
    }
    currentContext->buffer[currentContext->end] = object;
    currentContext->end++;
}

void collectGarbage(Context* context){
    int lastObject = context->end - 1;
    for(int i = lastObject; i >= 0; i--){
        free(currentContext->buffer[i]); 
    }
    context->end = 1;
}

Context* deleteContext(Context* context){
    collectGarbage(context);
    free(context);
    return NULL;
}