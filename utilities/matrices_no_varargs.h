/**
 * Andres Fuentes Hernandez
 * Libreria para manejar memoria y matrices
 **/
#ifndef _MATRICES_VARGS_
    #define _MATRICES_VARGS_
    #include <stdarg.h>
    #include <stdlib.h>
    #include <stdio.h>
    #include <tgmath.h>
    #include "memoria.h"

    #define M_PI 3.14159265358979323846

    typedef enum {
        BYTE,
        INTEGER,
        REAL
    } MatrixType;


    typedef struct{
        double* values;
        int rows;
        int columns;
        int isTransposed;
        MatrixType type;
    } Matrix2D;

    typedef Matrix2D Point;
    typedef Matrix2D Transform;
    typedef double Real;

    #define getPoint(x,y,z,q)   newPoint(4, 1,x,y,z,q)
    #define getX(p)   p->values[getIndex(p,0,0)]
    #define getY(p)   p->values[getIndex(p,1,0)]
    #define getZ(p)   p->values[getIndex(p,2,0)]
    #define getW(p)   p->values[getIndex(p,3,0)]
    #define setX(p, v)   (p->values[getIndex(p,0,0)] = v)
    #define setY(p, v)   (p->values[getIndex(p,1,0)] = v)
    #define setZ(p, v)   (p->values[getIndex(p,2,0)] = v)
    #define setW(p, v)   (p->values[getIndex(p,3,0)] = v)
    #define getTransform(...)   newMatrix(4, 4, __VA_ARGS__)
    #define getIndex(matrix, i, j) (matrix->isTransposed == 0? ( j + i*matrix->columns ) : ( i + j*matrix->rows ) )

    Matrix2D* zeros(unsigned rows, unsigned  columns);
    Matrix2D* newMatrix(unsigned rows, unsigned  columns, Real a,Real b,Real c,Real d,Real e,Real f,Real g,Real h,Real i,Real j,Real k, Real l, Real m, Real n, Real o, Real p);
    Matrix2D* newPoint(unsigned rows, unsigned  columns, Real x,Real y,Real z,Real q);
    void printMatrix(Matrix2D* matriz);
    Matrix2D* transpose(Matrix2D* matriz);

    Matrix2D* add(Matrix2D *restrict  matriz, double real);
    Matrix2D* multiply(Matrix2D *restrict matriz, double real);
    Matrix2D* divide(Matrix2D *restrict matriz, double real);

    Matrix2D* copy(Matrix2D *restrict matriz);
    Matrix2D* addMatrix(Matrix2D *restrict a, Matrix2D *restrict b);
    Matrix2D* subMatrix(Matrix2D *restrict a, Matrix2D *restrict b);
    Matrix2D* multiplyMatrix(Matrix2D *restrict a, Matrix2D *restrict b);
    Point* cross(Point *restrict a, Point *restrict b);
    double dot(Point *restrict a, Point *restrict b);
    Point* normalize(Point *restrict a);



#endif