#ifndef _OBJECTS_3D_
    #define _OBJECTS_3D_
    #include <tgmath.h>
    #include <stdarg.h>
    #include <limits.h>
    #include <float.h>
    #include "matrices_no_varargs.h"

    #define error 1e-10
    #define getBufferIndex(i, j, columns) (j + i*columns)
    #define getImageIndex(i, j,k, width, height) (i*4 + j*(width*4) + (2-k))
    #define max(a,b)  (a > b ? a : b)
    #define min(a,b)  (a > b ? b : a)
    #define RGB(red,green,blue) (blue + (green<<8) + (red<<16))
    #define R(color) ( color>>16)
    #define G(color) (color>>8 & 255)
    #define B(color)  (color & 255)
    #define RGBn(red,green,blue) ((int)(blue * 255.0) + (((int)(green * 255.0))<<8) + (((int)(red * 255.0))<<16))
    #define Rn(color) ((color>>16)/255.0)
    #define Gn(color) ((color>>8 & 255)/255.0)
    #define Bn(color)  ((color & 255)/255.0)
    #define mixColors(a,b) RGBn( (Rn(a) * Rn(b)), (Gn(a)*Gn(b)), (Bn(a)*Bn(b)) ) 
    typedef long Color;
    typedef enum {PHONG, GOURAUD, FLAT} Shading;

    struct ListNode{
        void* val;
        struct ListNode* next;
    };

    typedef struct ListNode List;

    typedef struct 
    {
        double maxX;
        double minX;
        double maxY;
        double minY;
        double maxZ;
        double minZ;
    } Box3D;

    typedef struct
    {
        int* vertexIndex;
        int size;
        int isVisible;
    } Face;

    typedef struct
    {   
        Point** vertex;
        Point** textels;
        Point** normals;
        Face** faces;
        Color* colors;
        Point* origen;
        double rotX;
        double rotY;
        double rotZ;
        int numVertex;
        int numFaces;
        Box3D* box;
        double ka;
        double kd;
        double ks;
        double ms; 
    } Object3D;
    

    typedef struct
    {   
        Point* up;
        Point* posicion;
        double rotX;
        double rotY;
        double rotZ;
        double farPlane;
        double nearPlane;
        double leftPlane;
        double rightPlane;
        double topPlane;
        double bottomPlane;
        double fov;
    } Camara;

    typedef struct{
        Point* origen;
        Point* normal;
        Color color;
    } Light;

    typedef struct{
        int width;
        int height;
        int numDiffuseLights;
        char* screenbuffer;
        double* zbuffer;
        Transform* lookAt;
        Transform* perspective;
        Transform* deviceMatrix;
        Light** diffuseLights;
        Light* ambientLight;
        Shading shading;
        unsigned int specularOn;
        unsigned int diffuseOn;
        unsigned int textureOn;
        Object3D* object;
    } ShaderContext;

    typedef struct 
    {
        double maxX;
        double minX;
        double maxY;
        double minY;
    } Box2D;


    typedef struct
    {
        double x;
        double y;
    } Pixel;
    
    

    Object3D* newEmptyObject3D(int numVertex, int numFaces);
    Face* newFace(int size, ...);
    Object3D* copyObject3D(Object3D* from);
    Transform* getLookAt(Camara* camara, Point* target);
    Transform* getObjectTransformation(Object3D* objecto);
    Transform* getPerspectiveProeyctionMatrix(Camara* objecto, double aspect);
    Transform* getOrtogonalProeyctionMatrix(Camara* camara, double aspect);
    Transform* getDeviceMatrix(double aspect, int width, int height);
    void createVertexNormals(Object3D* object);
    double* createZbuffer(int width, int height);
    char* createScreenBuffer(int width, int height);
    void vertexShading(ShaderContext* context);
    void fragmentShading(ShaderContext* context);
    void triangulateConvexFaces(Object3D* object);
    Object3D* readOBJFile(char* fileName);
    Face* copyFace(Face* face);
    Color renderColor(Color color, Point *restrict normal, Point *restrict eye, ShaderContext* context);
    void repositionObjectCamara(Object3D* object, Camara* camara);
    void createTextureCoordinates(Object3D* object);
    Color getTexture(double Xt, double Yt, double Zt);

#endif