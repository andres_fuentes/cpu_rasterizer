/**
 * Andres Fuentes Hernandez
 * Tarea02
 * MAC OS con Xquartz:   gcc  -O2 Graficos/motorGrafico.c  utilities .c -I/opt/X11/include/ -Iutilities/ -L/opt/X11/lib  -lX11 -o Graficos/motorGrafico && ./Graficos/motorGrafico dragon.obj 
 * Linux con librerias X11: gcc -O2 Graficos/motorGrafico.c  utilities{slash}*.c  -Iutilities -lX11 -lm  -o Graficos/motorGrafico  && ./Graficos/motorGrafico dragon.obj
 **/
#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <tgmath.h>
#include <string.h>
#include "../utilities/matrices_no_varargs.h"
#include "../utilities/memoria.h"
#include "../utilities/objects3D.h"


Object3D* testCube();
Object3D* testTriangle();

int main(int argc, char *argv[]){
    Context* globalContext = createContext(); // Creando la lista que contiene todos los punteros hacia los objetos utilizados en el programa
    setContext(globalContext);

     ///////// Inicializacion requerida por Xserver
    Window window;
    XEvent event;
    Display* display;
    GC context;
    unsigned int screen;
    int width, height;
    display = XOpenDisplay(NULL); // XOpenDisplay(getenv("DISPLAY")); // XOpenDisplay(":0.0");
    if(display == NULL){
        fprintf(stderr, "Verifique que XServer este corriendo y la variable DISPLAY exista");
        exit(EXIT_FAILURE);
    }

    width = 640;//DisplayWidth(display, screen)/2;
    height = 480;//DisplayHeight(display, screen)/2;
    int menuWidth = 300;
    screen = DefaultScreen(display);
    long white = WhitePixel (display, screen);
    long black = BlackPixel(display, screen);

    window = XCreateSimpleWindow(display, RootWindow(display, screen),
    0,0,width + menuWidth,height,
    10, white, black);

    context=XCreateGC(display, window, 0,0);   

    XSelectInput(display, window, ExposureMask | ButtonPressMask );
    XMapWindow(display, window);
    Visual *visual = DefaultVisual(display,screen);
    ////////////////////////////////////////////////

    Object3D* object =  readOBJFile(argv[1]); //Leer archivo obj

    Camara camara;
    camara.up = getPoint(0.0,1.0,0.0,1.0);
    camara.rotX = 0.0;
    camara.rotY = 0.0;
    camara.rotZ = 0.0;
    camara.farPlane = 1000.0;
    camara.nearPlane = 0.1;
    camara.fov = ((M_PI)/180)*60.0;
    double step = ((M_PI)/180)*20.0;
    double aspect = width/(double)height;
    repositionObjectCamara(object,&camara);


    Transform* perspective = getPerspectiveProeyctionMatrix(&camara, aspect);
    Transform* ortogonal = getOrtogonalProeyctionMatrix(&camara, aspect);
    Transform* proyeccion = perspective;
    Transform* deviceMatrix = getDeviceMatrix(aspect, width, height);
    Light* ambientLight  = newObject(sizeof(Light));
    ambientLight->color = RGB(255,255,255); 

    int numLights = 2;
    Light** difuseLights = newArrayObject(numLights, sizeof(Light*));
    difuseLights[0] = newObject(sizeof(Light));
    difuseLights[0]->color = RGB(255,255,0); 
    difuseLights[0]->origen = getPoint(-40.0,0.0,0.0,1.0);
    difuseLights[0]->normal = normalize(getPoint(1.0,0.0,0.0,0.0));

    difuseLights[1]  = newObject(sizeof(Light));
    difuseLights[1]->color = RGB(125,125,255); 
    difuseLights[1]->origen = getPoint(40.0,0.0,0.0,1.0);
    difuseLights[1]->normal = normalize(getPoint(-1.0,0.0,0.0,0.0));

    unsigned int specularOn = 1;
    unsigned int diffuseOn = 1; 
    unsigned int textureOn = 0;
    Shading shading = PHONG;

    Context* localContext = createContext();
    setContext(localContext);
    int repeat = 1;
    while (repeat)
    {
        int inicioMenuY = 0;
        int inicioMenuX = width;
        int buttonWidth = menuWidth/3;
        XNextEvent(display, &event);
        if (event.type == Expose ){ //Se dibuja el menu
            XSetForeground (display, context, RGB(0,0,161));
            XFillRectangle(display, window, context, inicioMenuX, inicioMenuY, inicioMenuX + menuWidth, inicioMenuY + height);
            int ybutton = inicioMenuY;
            int xbutton = inicioMenuX;
            int extra = buttonWidth/3;
            
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, buttonWidth-2, 50-2);
            char* button01 = "reset"; 
            XDrawString(display, window, context, xbutton + extra, ybutton+27, button01, strlen(button01));

            ybutton += 50;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, buttonWidth-2, 50-2);
            char* button02 = "left";
            XDrawString(display, window, context, xbutton + extra, ybutton+27, button02, strlen(button02));

            ybutton += 50;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, buttonWidth-2, 50-2);
            char* button11 = "Flat";
            XDrawString(display, window, context, xbutton + extra, ybutton+27, button11, strlen(button11));

            xbutton += buttonWidth;
            ybutton = inicioMenuY;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, buttonWidth-2, 50-2);
            char* button03 = "up";
            XDrawString(display, window, context, xbutton + extra, ybutton+27, button03, strlen(button03));

            ybutton += 50;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, buttonWidth-2, 50-2);
            char* button04 = "down";
            XDrawString(display, window, context, xbutton + extra, ybutton+27, button04, strlen(button04));

            ybutton += 50;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, buttonWidth-2, 50-2);
            char* button12 = "Gouraud";
            XDrawString(display, window, context, xbutton + extra, ybutton+27, button12, strlen(button12));

            xbutton += buttonWidth;
            ybutton = inicioMenuY;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, buttonWidth-2, 50-2);
            char* button05 = "[X]";
            XDrawString(display, window, context, xbutton + extra, ybutton+27, button05, strlen(button05));

            ybutton += 50;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, buttonWidth-2, 50-2);
            char* button06 = "right";
            XDrawString(display, window, context, xbutton + extra, ybutton+27, button06, strlen(button06));

            ybutton += 50;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, buttonWidth-2, 50-2);
            char* button13 = "Phong";
            XDrawString(display, window, context, xbutton + extra, ybutton+27, button13, strlen(button06));

            xbutton = inicioMenuX;
            ybutton = inicioMenuY + 150;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, (menuWidth/2)-2, 50-2);
            char* button07 = "Perspectiva";
            XDrawString(display, window, context, xbutton + (menuWidth/6), ybutton+27, button07, strlen(button07));

            xbutton += menuWidth/2;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, (menuWidth/2)-2, 50-2);
            char* button08 = "Ortogonal";
            XDrawString(display, window, context, xbutton + (menuWidth/6), ybutton+27, button08, strlen(button08));

            xbutton = inicioMenuX;
            ybutton = inicioMenuY + 200;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, (menuWidth/2)-2, 50-2);
            char* button09 = "Difusa (on/off)";
            XDrawString(display, window, context, xbutton + (menuWidth/12), ybutton+27, button09, strlen(button09));

            xbutton += menuWidth/2;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, (menuWidth/2)-2, 50-2);
            char* button10 = "Especular (on/off)";
            XDrawString(display, window, context, xbutton + (menuWidth/12), ybutton+27, button10, strlen(button10));

            xbutton = inicioMenuX;
            ybutton = inicioMenuY + 250;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, (menuWidth)-2, 50-2);
            char* button14 = "Textura (on/off)";
            XDrawString(display, window, context, xbutton + (menuWidth/3), ybutton+27, button14, strlen(button14));

            xbutton = inicioMenuX;
            ybutton = inicioMenuY + 300;
            XSetForeground (display, context, RGB(138,252,253));
            XDrawRectangle(display, window, context, xbutton, ybutton, (menuWidth)-2, 50-2);
            char* button15 = "Deformacion";
            XDrawString(display, window, context, xbutton + (menuWidth/3), ybutton+27, button15, strlen(button15));
        }
        if (event.type == ButtonPress){ // Si se recibe una accion del mouse aqui se determina que boton fue presionado
            int x = event.xbutton.x;
            int y = event.xbutton.y;
            switch (event.xbutton.button) {
                case Button1:
                    if( y > inicioMenuY && y < inicioMenuY + 50){
                        if(x > inicioMenuX  && x < buttonWidth + inicioMenuX){
                            camara.rotY = ((M_PI)/180)*0.0;
                            camara.rotX = ((M_PI)/180)*0.0;
                            object->rotX = ((M_PI)/180)*0.0;
                            object->rotY = ((M_PI)/180)*0.0;
                            object->rotZ = ((M_PI)/180)*0.0;
                        } else if( x > buttonWidth + inicioMenuX && x < 2*buttonWidth + inicioMenuX){
                            camara.rotX = camara.rotX > (2*M_PI-step)? 0.0: camara.rotX + step;
                        } else if((x > 2*buttonWidth + inicioMenuX) && x < inicioMenuX + menuWidth){
                            repeat = 0;
                        }
                    } else if ( y > inicioMenuY + 50 && y < inicioMenuY+100) {
                        if(x > inicioMenuX  && x < buttonWidth + inicioMenuX){
                            camara.rotY = camara.rotY < (-2*M_PI+step)? 0.0:camara.rotY - step;
                        } else if( x > buttonWidth + inicioMenuX && x < 2*buttonWidth + inicioMenuX){
                            camara.rotX = camara.rotX < (-2*M_PI+step)? 0.0: camara.rotX - step;
                        } else if((x > 2*buttonWidth + inicioMenuX) && x < inicioMenuX + menuWidth){
                            camara.rotY = camara.rotY > (2*M_PI-step)? 0.0: camara.rotY + step;
                        }
                    } else if ( y > inicioMenuY + 100 && y < inicioMenuY+150) {
                        if(x > inicioMenuX  && x < buttonWidth + inicioMenuX){
                            shading = FLAT;
                        } else if( x > buttonWidth + inicioMenuX && x < 2*buttonWidth + inicioMenuX){
                            shading = GOURAUD;
                        } else if((x > 2*buttonWidth + inicioMenuX) && x < inicioMenuX + menuWidth){
                            shading = PHONG;
                        }
                    } else if( y > inicioMenuY + 150 && y < inicioMenuY+200) {
                        if(x > inicioMenuX  && x < inicioMenuX + menuWidth/2){
                            proyeccion = perspective;
                        } else if(x > inicioMenuX + menuWidth/2){
                            proyeccion = ortogonal;
                        }
                    } else if( y > inicioMenuY + 200 && y < inicioMenuY+250) {
                        if( x > inicioMenuX  && x < inicioMenuX + menuWidth/2){
                            diffuseOn = !diffuseOn;
                        } else if(x > inicioMenuX + menuWidth/2) {
                            specularOn = !specularOn;
                        }
                    } else if( y > inicioMenuY+250 && y < inicioMenuY+300){
                        if( x > inicioMenuX){
                            textureOn = !textureOn;
                        }
                    }  else if( y > inicioMenuY+300 && y < inicioMenuY+350){
                        if( x > inicioMenuX){
                            object->rotZ += ((M_PI)/180)*45.0;
                        }
                    }
                    break;
                case Button2:
                    break;
                default:
                    break;
            }
        }
   if(!repeat){
       break;
   }

        Transform* lookAt = getLookAt(&camara, getPoint(0.0,0.0,0.0,1.0));
        char* screenBuffer = createScreenBuffer(width, height);
        double* zbuffer = createZbuffer(width, height);
        ShaderContext* shadingContext = newObject(sizeof(ShaderContext));
        shadingContext->lookAt = lookAt;
        shadingContext->perspective = proyeccion;
        shadingContext->deviceMatrix = deviceMatrix;
        shadingContext->numDiffuseLights = numLights;
        shadingContext->diffuseLights = difuseLights;
        shadingContext->ambientLight = ambientLight;
        shadingContext->shading = shading;
        shadingContext->diffuseOn = diffuseOn;
        shadingContext->specularOn = specularOn;
        shadingContext->height = height;
        shadingContext->width = width;
        shadingContext->screenbuffer = screenBuffer;
        shadingContext->zbuffer = zbuffer;
        shadingContext->object = object;
        shadingContext->textureOn = textureOn;

        vertexShading(shadingContext);
        fragmentShading(shadingContext); 

        XImage *img = XCreateImage(display,visual,DefaultDepth(display,screen),ZPixmap,
		0,screenBuffer,width,height,32,0);
        XPutImage(display,window,DefaultGC(display,screen),img,0,0,0,0,width,height);
        
        XFlush(display);
        collectGarbage(localContext);
    }
    localContext = deleteContext(localContext);
    setContext(globalContext);

    XDestroyWindow( display , window );
    XCloseDisplay(display);
    globalContext = deleteContext(globalContext);
    exit(EXIT_SUCCESS);
}

// Funciones de prueba en caso de no tener un archivo obj
Object3D* testCube(){
    Object3D* cubo = newEmptyObject3D(8, 6);
    /*
    v 15.0 -15.0 15.0
    v 15.0 15.0 15.0
    v -15.0 15.0 15.0
    v -15.0 -15.0 15.0
    v 15.0 -15.0 -15.0
    v 15.0 15.0 -15.0
    v -15.0 15.0 -15.0
    v -15.0 -15.0 -15.0
    */
    cubo->vertex[0] = getPoint( 15.0, -15.0, 15.0, 1.0);
    cubo->vertex[1] = getPoint( 15.0, 15.0, 15.0, 1.0);
    cubo->vertex[2] = getPoint( -15.0, 15.0, 15.0, 1.0);
    cubo->vertex[3] = getPoint( -15.0, -15.0, 15.0, 1.0);
    cubo->vertex[4] = getPoint( 15.0, -15.0, -15.0, 1.0);
    cubo->vertex[5] = getPoint( 15.0, 15.0, -15.0, 1.0);
    cubo->vertex[6] = getPoint( -15.0, 15.0, -15.0, 1.0);
    cubo->vertex[7] = getPoint( -15.0, -15.0, -15.0, 1.0);

    cubo->faces[0] = newFace(4, 0, 1, 2, 3);
    cubo->faces[1] = newFace(4, 1, 0, 4, 5);
    cubo->faces[2] = newFace(4, 5, 4, 7, 6);
    cubo->faces[3] = newFace(4, 6, 7, 3, 2);
    cubo->faces[4] = newFace(4, 1, 5, 6, 2);
    cubo->faces[5] = newFace(4, 0, 3, 7, 4);

    cubo->colors[0] = RGB(255,255,255);
    cubo->colors[1] = RGB(255,255,255);
    cubo->colors[2] = RGB(255,255,255);
    cubo->colors[3] = RGB(255,255,255);
    cubo->colors[4] = RGB(255,255,255);
    cubo->colors[5] = RGB(255,255,255);
    cubo->colors[6] = RGB(255,255,255);
    cubo->colors[7] = RGB(255,255,255);

    triangulateConvexFaces(cubo);
    createVertexNormals(cubo);
    return cubo;
}

Object3D* testTriangle(){
    Object3D* triangle = newEmptyObject3D(3, 1);
    /*
    v 15.0 -15.0 15.0
    v 15.0 15.0 15.0
    v -15.0 15.0 15.0
    v -15.0 -15.0 15.0
    v 15.0 -15.0 -15.0
    v 15.0 15.0 -15.0
    v -15.0 15.0 -15.0
    v -15.0 -15.0 -15.0
    */
    triangle->vertex[0] = getPoint( -15.0, -15.0, 15.0, 1.0);
    triangle->vertex[1] = getPoint( 15.0, -15.0, 15.0, 1.0);
    triangle->vertex[2] = getPoint( 15.0, 15.0, 15.0, 1.0);
   

    triangle->faces[0] = newFace(3, 0, 1, 2);

    triangle->colors[0] = RGB(255,0,0);
    triangle->colors[1] = RGB(0,255,0);
    triangle->colors[2] = RGB(0,0,255);


    triangulateConvexFaces(triangle);
    createVertexNormals(triangle);
    createTextureCoordinates(triangle);
    return triangle;
}
