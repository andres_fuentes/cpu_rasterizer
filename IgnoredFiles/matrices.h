/**
 * Andres Fuentes Hernandez
 * Libreria para manejar memoria y matrices
 **/
#ifndef _MATRICES_
    #define _MATRICES_
    #include <stdarg.h>
    #include <stdlib.h>
    #include <stdio.h>
    #include <math.h>
    #include "memoria.h"

    typedef enum {
        BYTE,
        INTEGER,
        REAL
    } MatrixType;


    typedef struct{
        double* values;
        int rows;
        int columns;
        int isTransposed;
        MatrixType type;
    } Matrix2D;

    typedef Matrix2D Point;
    typedef Matrix2D Transform;

    #define getPoint(...)   newMatrix(4, 1, __VA_ARGS__)
    #define getX(p)   p->values[getIndex(p,0,0)]
    #define getY(p)   p->values[getIndex(p,1,0)]
    #define getZ(p)   p->values[getIndex(p,2,0)]
    #define getW(p)   p->values[getIndex(p,3,0)]
    #define setX(p, v)   (p->values[getIndex(p,0,0)] = v)
    #define setY(p, v)   (p->values[getIndex(p,1,0)] = v)
    #define setZ(p, v)   (p->values[getIndex(p,2,0)] = v)
    #define setW(p, v)   (p->values[getIndex(p,3,0)] = v)
    #define getTransform(...)   newMatrix(4, 4, __VA_ARGS__)
    #define getIndex(matrix, i, j) (matrix->isTransposed == 0? ( j + i*matrix->columns ) : ( i + j*matrix->rows ) )

    Matrix2D* zeros(unsigned rows, unsigned  columns);
    Matrix2D* newMatrix(unsigned rows, unsigned  columns, ...);
    void printMatrix(Matrix2D* matriz);
    Matrix2D* transpose(Matrix2D* matriz);

    Matrix2D* add(Matrix2D* matriz, double real);
    Matrix2D* multiply(Matrix2D* matriz, double real);
    Matrix2D* divide(Matrix2D* matriz, double real);

    Matrix2D* copy(Matrix2D* matriz);
    Matrix2D* addMatrix(Matrix2D* a, Matrix2D* b);
    Matrix2D* subMatrix(Matrix2D* a, Matrix2D* b);
    Matrix2D* multiplyMatrix(Matrix2D* a, Matrix2D* b);
    Point* cross(Point* a, Point* b);
    double dot(Point* a, Point* b);
    Point* normalize(Point* a);



#endif